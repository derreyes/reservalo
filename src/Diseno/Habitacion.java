
package Diseno;

import java.io.Serializable;
import java.util.ArrayList;
/**
 *
 * @author Henry Fernández
 */
public class Habitacion implements Serializable {
     
    private double precio;
    private String tipo;  //el tipo es el detalle de la habitacion: matromonial, sencilla, etc
    private int capacidad,camas, cantidad;
    private String[] servicios;

    
    public Habitacion(double precio, String tipo, int capacidad, int camas, String[] servicios,int cantidad) {
        this.precio = precio;
        this.tipo = tipo;
        this.capacidad = capacidad;
        this.camas = camas;
        this.servicios = servicios;
        this.cantidad=cantidad;
    }
    
    //metodos de Getters y Setters 
 public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public int getCamas() {
        return camas;
    }

    public void setCamas(int camas) {
        this.camas = camas;
    }

    public String[] getServicios() {
        return servicios;
    }

    public void setServicios(String[] servicios) {
        this.servicios = servicios;
        }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    //Se sobreescribe el metodo toString para q nos facilite la lectura de la Habitacion
    @Override
    public String toString() {
        return "Habitacion{" + " tipo= " + tipo + ", precio= " + precio + ", capacidad= " + capacidad + ", camas= " + camas + ", cantidad de habitaciones dsiponibles= " + cantidad + ", servicios= " + servicios + '}';
    }
    


    }
    

 
