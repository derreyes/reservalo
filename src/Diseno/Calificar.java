
package Diseno;

/**
 *
 * @Sueanny Moreno
 */
public class Calificar {
    private int limpieza,ubicacion,precio,
            wifi,confort,personal;
    private double promedio;
    
    public Calificar(){
    }

    public Calificar(int limpieza, int ubicacion, int precio, int wifi, int confort, int personal) {
        this.limpieza = limpieza;
        this.ubicacion = ubicacion;
        this.precio = precio;
        this.wifi = wifi;
        this.confort = confort;
        this.personal = personal;
        double suma=0;
        suma=(confort + limpieza + personal +precio +ubicacion +wifi)/6;
        this.promedio =suma;
    }

    public int getLimpieza() {
        return limpieza;
    }

    public void setLimpieza(int limpieza) {
        this.limpieza = limpieza;
    }

    public int getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(int ubicacion) {
        this.ubicacion = ubicacion;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getWifi() {
        return wifi;
    }

    public void setWifi(int wifi) {
        this.wifi = wifi;
    }

    public int getConfort() {
        return confort;
    }

    public void setConfort(int confort) {
        this.confort = confort;
    }

    public int getPersonal() {
        return personal;
    }

    public void setPersonal(int personal) {
        this.personal = personal;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio() {
        double suma=0;
        suma=(this.confort + this.limpieza + this.personal +this.precio +this.ubicacion +this.wifi)/6;
        this.promedio =suma;
    }
    

    
}
