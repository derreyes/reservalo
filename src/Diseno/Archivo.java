package Diseno;
import InterfazUsuario.UIMain;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
 
/**
 *
 * @Sueanny Moreno
 */
public  class Archivo {
     /**
      * Este metodo permite leer un archivo csv y devuelve una lista de lista con cada una de las lineas del archivo y sus valores 
      * @param nombre.....
      * @return .....
      */
    public List<ArrayList<String>> leerArchivo(String nombre){
        List<ArrayList<String>> listaLineas=new ArrayList<>();
        //nombre= "C:\\Users\\HP\\Desktop\\Espol\\4to Semestre\\POO\\simplemaps-worldcities-basic.csv";
     BufferedReader br = null;
     String line = "";
     //Se define separador ","
          try {
         br = new BufferedReader(new FileReader(nombre));
         while ((line = br.readLine()) != null) {                
             ArrayList<String> listaPalabras = new ArrayList<>();         
             String[] separaLinea=line.split(",");
             listaPalabras.addAll(Arrays.asList(separaLinea));
             listaLineas.add(listaPalabras);
         }  
     } catch (FileNotFoundException e) {
         e.printStackTrace();
     } catch (IOException e) {
         e.printStackTrace();
     } finally {
         if (br != null) {
             try {
                 br.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
}   
          return listaLineas;
    }
    /**
     * Este metodo recibe la direccion de un archivo txt y devuelve un arreglo con los datos del archivo.
     * @param direccion.....
     * @return ...
     */
 public String[] leerinfor(String direccion){ 
    String cadena="";
    File file = new File(direccion);
    FileReader fileR = null;
    BufferedReader file2 = null;
try {
    fileR = new FileReader(file);
    file2 = new BufferedReader(fileR);

} catch (FileNotFoundException e) {
    System.out.println("No se encontro el archivo "+file.getName());
}
try {
    String lines; 
    while((lines = file2.readLine()) != null)
        {
            cadena+=" , "+lines;
        }
} catch (IOException e) {
    e.printStackTrace();
}
String[] separado=cadena.split(", ");
 return separado;}
 
}

