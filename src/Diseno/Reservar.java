package Diseno;

import Usuario.Usuario;
import Usuario.UsuarioViajero;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import javax.mail.internet.ParseException;
/**
 *
 * @author Dario Erreyes
 */
public class Reservar {
    private Usuario user;
    private int noches;
    private Establecimiento establecimiento;
    private int cantidadHabitacion;
    private String tipo;
    private Date fechaInicio,fechaFinal;
    private double precio;
    private ArrayList<Reservar> reservas=new ArrayList<>();

    public Reservar(){
        
    }

    public Reservar(Usuario user, int noches, Establecimiento establecimiento, int cantidadHabitacion, String tipo, Date fechaInicio, Date fechaFinal, double precio) {
        this.user = user;
        this.noches = noches;
        this.establecimiento = establecimiento;
        this.cantidadHabitacion = cantidadHabitacion;
        this.tipo = tipo;
        this.fechaInicio = fechaInicio;
        this.fechaFinal = fechaFinal;
        this.precio = precio;
    }  

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public int getNoches() {
        return noches;
    }

    public void setNoches(int noches) {
        this.noches = noches;
    }

    public Establecimiento getEstablecimiento() {
        return establecimiento;
    }

    public void setEstablecimiento(Establecimiento establecimiento) {
        this.establecimiento = establecimiento;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public ArrayList<Reservar> getReservas() {
        return reservas;
    }

    public void setReservas(Reservar reservas) {
        this.reservas.add(reservas);
    }

    

    static Reservar r=new Reservar();
    /**
     * Este metodo me permite crear la reserva y almacenarla en una lista.
     * @throws ParseException
     * @throws java.text.ParseException 
     */
    public void reservar(Reservar rr,Usuario user, Establecimiento establecimiento, String fecha,  String tipo, int cantidad,int noches) throws ParseException, java.text.ParseException{
        Scanner sc=new Scanner(System.in);
        Habitacion habs=null;
        for (Habitacion hab: establecimiento.getHabitaciones()){
            if(hab.getTipo().equalsIgnoreCase(tipo)){
                habs=hab;
            }
        }
        Date fechaFinal= rr.sumarDias(fecha, noches);
        double precio=rr.calcularPrecio(user, noches,habs);
        System.out.println("El precio total de su reservacion es de: " + precio);
        DateFormat format = new SimpleDateFormat("DD/MM/YYYY"); // Creamos un formato de fecha
        Date Inicio = format.parse(fecha); // Creamos un date con la entrada en el formato especificado
        
        System.out.print("Desea realizar su reservacion \"SI\" o \"NO\": ");
        String resp=sc.nextLine().toUpperCase().trim();
        while (!"SI".equals(resp)&&!"NO".equals(resp)){
             System.out.println("Opción no valida");
             System.out.print("Ingrese de nuevo la opción: ");
             resp=sc.nextLine().toUpperCase().trim();
        }
        if ("SI".equals(resp)){
            Reservar reserva=new Reservar(user,noches,establecimiento,cantidad,tipo,Inicio,fechaFinal,precio);
            reservas.add(reserva);
            System.out.println("Su reserva fue anadida a la lista con exito ");
            reserva.restarHabitaciones(establecimiento, tipo, cantidad);
            reserva.setReservas(reserva);
            System.out.println("Su reservacion ha sido guardada, pase Buen Dia :D \n");
        }
        else System.out.println("Vuelva pronto. \n");
        
    }

    /**
     * 
     * @param fecha///
     * @param dias////
     * @return///
     * @throws ParseException///
     * @throws java.text.ParseException ////
     * Se crea un formato de fecha, un date con la entrada en el formato especificado
     * y se recibe la fecha que se recibe, numero de días a añadir
     * Devuelve el objeto Date con los nuevos días añadidos
     */
    public static Date sumarDias(String fecha, int dias) throws ParseException, java.text.ParseException{
    DateFormat format = new SimpleDateFormat("DD/MM/YYYY"); 
    Date newfecha = format.parse(fecha); 
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(newfecha); 
    calendar.add(Calendar.DAY_OF_WEEK, dias);  
      return calendar.getTime();
 }

    /**
     * Este metodo me permite calcular el precio, haciendole un descuento del 10% si el usuario es de tipo genius
     * @param user
     * @param noches
     * @param hab
     * @return 
     */
    public double calcularPrecio(Usuario user, int noches, Habitacion hab){
        if (user instanceof UsuarioViajero){
            UsuarioViajero u= (UsuarioViajero) user;
            if (u.getEsGenius()){
                return hab.getPrecio()*noches*0.9;
            }
            else{
                return hab.getPrecio()*noches;
            }
        }
        else {
            return hab.getPrecio()*noches;
        }
    }
    
/**
 * Este metodo permite mostrar las habitaciones que posee un establecimiento 
 * @param e 
 */
    public void mostrarHabitaciones(Establecimiento e){
        Scanner sc = new Scanner(System.in);
        ArrayList<Habitacion> h=e.getHabitaciones();
        for (Habitacion l:h){
            String servicios=" ";
            for(String s:l.getServicios()){
                servicios+=s + ", ";}
            System.out.println(l.toString().replace(l.getServicios().toString(), servicios));
            
                    }
        
     }
    
    /**
     * este metodo me permite cancelar la reserva, eliminandola de la lista
     * @param user 
     */
    
    public void cancelarReserva(String user){
        Reservar r=new Reservar();
        Scanner sc = new Scanner(System.in);
        ArrayList<Reservar> reserv = new ArrayList<>();
        ArrayList<String> est=new ArrayList<>() ;
        int n=1;
        for(Reservar u:reservas){
            
            if((u.getUser().getNombre()).equals(user.toUpperCase())){
                System.out.println("Reserva "+ n +" en el "+u.getEstablecimiento().getTipo()+
                    " "+u.getEstablecimiento().getNombre()+" para " +r.getFechaFinal()+" hasta "+r.getFechaFinal());
                reserv.add(u);
                }
            n++;
        }
        
        System.out.println("Elija el numero de reserva que desea cancelar");
        String respuesta=sc.nextLine();
        Validar v=new Validar();
        while(! v.validarDatoNumerico(respuesta)){
        System.out.println("El dato que ingreso no es un numero entero ");
        System.out.print("Elija nuevamente el numero de reserva: ");
        respuesta=sc.nextLine().trim();      }
        int numReserva=Integer.parseInt(respuesta);
        if (numReserva<=reserv.size()){
            reservas.remove(reserv.get(numReserva-1));
            System.out.println("______Su reserva ha sido cancelada con exito___");
        }}
    
    /**
     * este metodo me permite disminuir las habitaciones cuando se realice la reserva
     * @param e
     * @param tipo
     * @param cantidad 
     */
    public void restarHabitaciones(Establecimiento e,String tipo, int cantidad){
        for (Habitacion hab: e.getHabitaciones()){
            if(hab.getTipo().equalsIgnoreCase(tipo)){
                int num=hab.getCantidad()-cantidad;
                hab.setCantidad(num);
            }
        }
    }
    }
