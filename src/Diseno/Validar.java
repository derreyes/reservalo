package Diseno;

/**
 *
 * @Grupo 
 */


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validar {

/**
 * Este parametro recibe por parameto el String de contrasena y corrobora que esta cumpla con 
 * la condicion de longitud y que tenga cierto tipo de caracteres. Devuelve un dato boolean
 * @param contrasena///
 * @return ////
 */
      public boolean validarContrasena(String contrasena){
        if (contrasena.length()>=6){
            int cont1=0;
            int cont2=0;
            for (int n=0;n<contrasena.length();n++){
                char c=contrasena.charAt(n);
                if (Character.isDigit(c)){
                    cont1++;                    
                }
                if(Character.isLetter(c)){
                    cont2++;
                }
            }
            if (cont1>=1&&cont2==contrasena.length()-cont1){
                return true;
            }
        }
        return false; 
    }
    
/**
 * Este metodo recibe un correo con el tipo String y corrobora que sea uno retornando un dato boolean
 * @param correo/////
 * @return /////
 */
    public boolean validarCorreo(String correo){
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(correo);
        return mather.find() == true; 
            }

/**
 * Este m[etodo recibe por parameto un String y devuelve un boolean de si el string
 * representa o no a un numero entero.
 * @param dato/////
 * @return /////
 */    
    public boolean validarDatoNumerico(String dato){
        int cont1=0;
        for (int n=0;n<dato.length();n++){
                char c=dato.charAt(n);
                if (Character.isDigit(c)){
                    cont1++;                    
                }
            }
        
          return cont1==dato.length();
    }
    
    /**
     * Este metodo se encarga de validar si el dato ingresado por el usuario es un double
     * @param dato////
    *Funcionamiento: //////
    *Recibe el dato e intenta convertirlo en double con parseDouble, en caso de que se pueda retorna un
    true, pero si no puede va al catch y retorna un false.
     * @return ///////
     */
    public boolean validarDatoDouble(String dato){
            try
      {
        Double.parseDouble(dato);
        return true;
      }
      catch(NumberFormatException nfe)
      {
        return false;
      }
      }
   }

