package Diseno;

import java.util.ArrayList;
import java.util.List;
import Diseno.*;
/**
 *
 * @Sueanny Moreno
 */
public class Atraccion {
    
    /** 
     * Este metodo recibe una ciudad y atraccion y devuelve un arreglo con la atraccion y sus coordenadas
     * para luego ser usada por otro metodo y determinar los hoteles que estan cerca de esa atraccion
     * @param ciudad
     * @param atraccion
     * @return 
     */
    public static ArrayList<String> buscarAtracciones(String ciudad, String atraccion){
        Archivo a=new Archivo();
        ArrayList<String> atracLonLat= new ArrayList<>();
        List<ArrayList<String>> atDoc=a.leerArchivo("atracciones.csv");
        for(ArrayList<String> inf:atDoc){
            if (((inf.get(2)).equalsIgnoreCase(atraccion))){
                atracLonLat.add(inf.get(2)); //atraccion
                atracLonLat.add(inf.get(3));  //latitud
                atracLonLat.add(inf.get(4));  //longitud
                System.out.println("Se extrajo la atraccion con exito");
                }
       }
        if(atracLonLat.size()==0){
            System.out.println("Lo sentimos, no hay atracciones registradas para la ciudad");}
        return atracLonLat ;
    }
}
    


