/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Diseno;
import InterfazUsuario.UIMain;
import Usuario.Usuario;
import Usuario.UsuarioAlojamiento;
import Usuario.UsuarioViajero;
import java.io.*;
import java.util.*;

/**
 * @Sueanny Moreno
 */
public class Establecimiento implements Serializable{
 private String nombre,tipo,ciudad,pais;
 private long ruc;
 private int estrellas, cantidadHab;
 private double longitud;
 private double latitud;
 private ArrayList<Habitacion> habitaciones;
 private String resenas="";
 private double PomedioPrecio=0;
 private double calificacion=0;
 private ArrayList<Double> calificaciones=new ArrayList<>();
 private double distAtraccion;

    public Establecimiento(){};
 
    public Establecimiento(String nombre, String tipo, String ciudad, String pais, long ruc, int estrellas, int cantidadHab , double longitud, double latitud, ArrayList<Habitacion> habitaciones) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.ciudad = ciudad;
        this.pais = pais;
        this.ruc = ruc;
        this.cantidadHab=cantidadHab;
        this.estrellas = estrellas;
        this.longitud = longitud;
        this.latitud = latitud;
        this.habitaciones = habitaciones;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public long getRuc() {
        return ruc;
    }

    public void setRuc(long ruc) {
        this.ruc = ruc;
    }

    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public ArrayList<Habitacion> getHabitaciones() {
        return habitaciones;
    }

    public void setHabitaciones(ArrayList<Habitacion> habitaciones) {
        this.habitaciones = habitaciones;
    }

    public int getCantidadHab() {
        return cantidadHab;
    }

    public void setCantidadHab(int cantidadHab) {
        this.cantidadHab = cantidadHab;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    public String getResena() {
        return resenas;
    }

    public double getPomedioPrecio() {
        return PomedioPrecio;
    }

    public void setPomedioPrecio(double PomedioPrecio) {
        this.PomedioPrecio = PomedioPrecio;
    }

    public ArrayList<Double> getCalificaciones() {
        return calificaciones;
    }

    public void setResena(String resenas) {
        this.resenas = resenas;
    }

    public void setCalificaciones(ArrayList<Double> calificaciones) {
        this.calificaciones = calificaciones;
    }

   


    public double getDistAtraccion() {
        return distAtraccion;
    }

    public void setDistAtraccion(double distAtraccion) {
        this.distAtraccion = distAtraccion;
    }

    /**
     * Este metodo se encarga de soobreescribir el objeto Establecimiento
     * para que puedan mostrarse los valores de cada uno de sus atributos con su respectiva etiqueta
     * mas no la referencia del objeto
     */
 
   
    @Override
    public String toString(){
  
        return "Establecimiento{" + "nombre=" + nombre + ", tipo=" + tipo + ", ciudad=" + ciudad + ", pais=" + pais + ", ruc=" + ruc + ", estrellas=" + estrellas + ", cantidadHab=" + cantidadHab + ", longitud=" + longitud + ", latitud=" + latitud + ", habitaciones=" + habitaciones + ", resena=" + resenas + ", PomedioPrecio=" + PomedioPrecio + ", calificacion=" + calificacion + ", calificaciones=" + calificaciones + ", distAtraccion=" + distAtraccion + '}';
    }

    public void anadirEstablecimiento(Establecimiento E) throws IOException {
        ArrayList<Establecimiento> establecimientos= new ArrayList<>();
        
        try(ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("Establecimiento.txt")))){
            Object o;
            if ((o=leer.readObject())!=null){
                establecimientos=(ArrayList<Establecimiento>) o;            
            }
            leer.close();
        }catch(ClassNotFoundException | IOException  ex){}
        
        establecimientos.add(E);
        try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Establecimiento.txt")))){
            escribir.writeObject(establecimientos);
            System.out.println("Se ha registrado su Establecimientos exitosamente!!\n");
            escribir.close();
        }catch(IOException e){System.out.println(e.getMessage());}
    }
    
    /**
     * Este metodo crea una ista de establecimeintos y tambien crea un archivo de texto para almacenar dichos establecimeintos
     * @return//
     * @throws IOException //
     */
    public ArrayList<Establecimiento> crearListaEstablecimiento() throws IOException{
        ArrayList<Establecimiento> establecimientos = new ArrayList<>() ;
        try(ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("Establecimiento.txt")))){
            Object o;
            if ((o=leer.readObject())!=null){
                establecimientos=(ArrayList<Establecimiento>) o;           
            }
            leer.close();
        }catch(ClassNotFoundException | IOException  ex){}
        return establecimientos;
    }
    
    /**
     * Este metodo permite que el usuario una vez culminada su instancia pueda anadir una resena
     * del lugar en el que hospedo
     * @param e//
     * @param resena//
     * @throws IOException //
     */
    public void anadirResena(Establecimiento e,String resena) throws IOException{
        ArrayList<Establecimiento> estabs= e.crearListaEstablecimiento();
         ArrayList<Establecimiento> est=new ArrayList<Establecimiento>();
        for (Establecimiento o:estabs){
            if (!(e.getRuc()==o.getRuc())) {
                est.add(o);
            }
            else{
                String resenas= e.getResena();
                resenas+= "/" + resena;
                e.setResena(resenas);
                est.add(e);
            }
        }
        try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Establecimiento.txt")))){
        escribir.writeObject(est);
        escribir.close();
        }catch(IOException s){}
    }
    
    /**
     * Este metodo permite ordenar a los establecimiento segun varios criterios, como lo son
     * el numero de estrellas, el precio promedio de la estadia en el establecimiento y su distancia a 
     * determinada atraccion turistica de la ciudad.
     * Retorna una lista de establecimientos 
     * @return//
     * @throws IOException //
     */
    public ArrayList<Establecimiento> ordenarEstablecimiento() throws IOException{
        Scanner sc=new Scanner(System.in);  
        ArrayList<Establecimiento> establecimientos=crearListaEstablecimiento();
        String opcion="";
        while(!(opcion).equals("2")&&!(opcion).equals("1")&&!(opcion).equals("3")&&!(opcion).equals("4")){
                System.out.println("Desea ordenar los establecimientos por:");
                System.out.println("1.Estrellas");
                System.out.println("2.Precio");
                System.out.println("3.Calificacion");
                System.out.println("4.Cercania a atraccion de la ciudad");
                System.out.print("Ingrese opción:");
                opcion=sc.next();
                switch (opcion){
                    case "1":
                        Collections.sort(establecimientos, new Comparator<Establecimiento>() {
                    @Override
                    public int compare(Establecimiento o1, Establecimiento o2) {
                        return (String.valueOf(o2.getEstrellas())).compareTo(String.valueOf(o1.getEstrellas()));
                    }
        });
                        break;
                    case "2":
                        for(Establecimiento e:establecimientos){
                            int n=0;
                            double suma=0;
                            double preProm=0;
                            for (Habitacion hab:e.getHabitaciones()){
                                suma+=hab.getPrecio();
                                n++;
                            }
                            preProm=suma/n;
                            e.setPomedioPrecio(preProm);
                              }
                        Collections.sort(establecimientos, new Comparator<Establecimiento>() {
                    @Override
                    public int compare(Establecimiento o1, Establecimiento o2) {
                        return (String.valueOf(o2.getPomedioPrecio())).compareTo(String.valueOf(o1.getEstrellas()));
                    }}
        );
                        break;
                    case "3":
                        Collections.sort(establecimientos, (Establecimiento o1, Establecimiento o2) -> (String.valueOf(o2.getCalificacion())).compareTo(String.valueOf(o1.getCalificacion())));
                    break;
                    case "4":
                        System.out.println("Ingrese la ciudad de consulta: ");
                        String ciudad=sc.next().trim();
                        System.out.println("Ingrese la atraccion que desea buscar: ");
                        String atraccion=sc.next().trim();
                        ArrayList<String> lista= Atraccion.buscarAtracciones(ciudad,atraccion);
                        if (lista.size()>0){    
                        double latA=Double.parseDouble(lista.get(1));
                            double lonA=Double.parseDouble(lista.get(2));
                            for(Establecimiento e:establecimientos){
                               double lat=e.getLatitud();
                               double lon=e.getLongitud();
                          double dist = Math.sqrt(Math.pow((lat-latA),2)+Math.pow((lon-lonA),2));
                          e.setDistAtraccion(dist);
                                  }}
                          Collections.sort(establecimientos, new Comparator<Establecimiento>() {
                        @Override
                        public int compare(Establecimiento o1, Establecimiento o2) {
                            return (String.valueOf(o1.getDistAtraccion())).compareTo(String.valueOf(o2.getDistAtraccion()));
                        }}
                        );
                     break;
                    default:
                        System.out.println("Opcion no valida!!");
                        
                }
                        
        }
    return establecimientos;
    }
    /**
     * Este metodo permite calcular el promedio general de la calificacion para un establecimiento
     * @param califs //
     */
    public void calcularcalificacion(ArrayList<Double> califs, Establecimiento e){
        double suma=0;
        for(double c:califs){
            suma+=c;}
        e.setCalificacion(suma/(califs.size()));
    }
    /** 
     * Este metodo permite que el usuario pueda anadir una calificacion a un hotel
     * @param c //
     */
    public void anadirCalificacion(Calificar c, Establecimiento e) throws IOException{
        ArrayList<Establecimiento> estabs= e.crearListaEstablecimiento();
        ArrayList<Establecimiento> est=new ArrayList<Establecimiento>();
        for (Establecimiento o:estabs){
            if (!(e.getRuc()==o.getRuc())) {
                est.add(o);
            }
            else{
                System.out.println(c.getPromedio());
                ArrayList<Double> califi=e.getCalificaciones();
                califi.add(c.getPromedio());
                e.setCalificaciones(califi);
                e.calcularcalificacion(califi, e);
                est.add(e);
            }
        
        }
        try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Establecimiento.txt")))){
        escribir.writeObject(est);
        escribir.close();
        }catch(IOException s){}
    }
    
    /**
     * permite que el usuario pueda buscar un establecimiento y sus atributos al ingresar su nombre.
     * @param nombre
     * @return
     * @throws IOException 
     */
    public Establecimiento buscarEstablecimiento(String nombre) throws IOException{
        Establecimiento e=new Establecimiento();
        ArrayList<Establecimiento> estab=e.crearListaEstablecimiento();
        for (Establecimiento E:estab){  
            if (E.getNombre().equalsIgnoreCase(nombre))
                return E;
        }
        return null;
    }
    
    /**
     * Permite que el usuario pueda encontrar habitaciones en el establecimiento que quiera
     * @param e//
     * @param nombre//
     * @return //
     */
    
    public Habitacion buscarHabitaciones(Establecimiento e, String nombre){
        for (Habitacion h:e.getHabitaciones()){
            if (nombre.equalsIgnoreCase(h.getTipo())){
                return h;
            }
                
        }
        return null;
    }
            
}
