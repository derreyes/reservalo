/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;

/**
 *
 * @author Dario Erreyes
 */
public class UsuarioViajero extends Usuario {
    private boolean esGenius;

    public UsuarioViajero( String nombre, String correo, String contrasena, long tarjetaCredito,boolean esGenius) {
        super(nombre, correo, contrasena, tarjetaCredito);
        this.esGenius = esGenius;
    }

    public boolean getEsGenius() {
        return esGenius;
    }

    public void setEsGenius(boolean esGenius) {
        this.esGenius = esGenius;
    }

    @Override
    public String toString() {
        return "UsuarioViajero{" +super.toString()+ "esGenius=" + esGenius + '}';
    }

    /**
     * 
     * @return 
     */


    

    
}
