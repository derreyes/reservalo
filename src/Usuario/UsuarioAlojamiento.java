
package Usuario;

import Diseno.Establecimiento;
import java.io.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author Dario Erreyes
 */
public class UsuarioAlojamiento extends Usuario{
    private ArrayList<Establecimiento> establecimientos =new ArrayList<>();
    
    public UsuarioAlojamiento(String nombre, String correo, String contrasena, long tarjetaCredito) {
        super(nombre, correo, contrasena, tarjetaCredito);
    }

    public ArrayList<Establecimiento> getEstablecimientos() {
        return establecimientos;
    }

    public void setEstablecimientos(ArrayList<Establecimiento> establecimientos) {
        this.establecimientos = establecimientos;
    }


    @Override
    public String toString() {
        return "UsuarioAlojamiento{" +super.toString()+ "establecimientos=" + establecimientos + '}';
    }
    
    public void agregarEstablecimientoUsuario(Usuario u,Establecimiento e) throws IOException{
        ArrayList<Usuario> userss= u.crearListaUser();
        ArrayList<Usuario> USSS= new ArrayList<>();
        for (Usuario o:userss){
            if (!(u.getCorreo().equals(o.getCorreo()))) {
                USSS.add(o);
            }
            else{
                ArrayList<Establecimiento> est= ((UsuarioAlojamiento) o).getEstablecimientos();
                est.add(e);
                ((UsuarioAlojamiento) o).setEstablecimientos(est);
                USSS.add(o);
            }
        }
        try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Usuario.txt")))){
            escribir.writeObject(USSS);
            escribir.close();
        }catch(IOException s){}
    }
    
}
