/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuario;
import java.io.*;
import java.util.ArrayList;
/**
 *
 * @author Dario Erreyes
 * Esta clase permite crear un objeto tipo usuario el cual contara con los atributos de cada uno.
 */
public class Usuario implements Serializable{
    private String nombre, correo, contrasena;
    private long tarjetaCredito;

    public Usuario(){};
    
    /**
     * 
     * @param nombre: El nombre del usuario sera de tipo String 
     * @param correo: el correo sera de tipo String
     * @param contrasena: La contrasena sera de tipo String
     * @param tarjetaCredito: La tarjeta sera de tipo long.
     * 
     */
    public Usuario(String nombre, String correo, String contrasena, long tarjetaCredito) {
        this.nombre = nombre;
        this.correo = correo;
        this.contrasena = contrasena;
        this.tarjetaCredito = tarjetaCredito;
    }

    /*
     Se crean los Getters y Setters de cada atributo.
     */
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public long getTarjetaCredito() {
        return tarjetaCredito;
    }

    public void setTarjetaCredito(long tarjetaCredito) {
        this.tarjetaCredito = tarjetaCredito;
    }

    /**
     * Sobrescribimos el metodo toString para Usuario
     * @return: Retorna un String donde se encontrara cada atributo del Usuario.
     */
    @Override
    public String toString(){
        return "nombre=" + nombre + ", correo=" + correo + ", contrasena=" + contrasena + ", tarjetaCredito=" + tarjetaCredito +" ";
    }

    /**
     * Este metodo nos permite aanadir a un Usuario a un archivo de tipo TXT.
     * @param user: es el Usuario q se desea agregar.
     * @throws IOException //
     */
    public void añadirUsuario(Usuario user) throws IOException {
        ArrayList<Usuario> users= new ArrayList<>();
        
        try(ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("Usuario.txt")))){
            Object o=leer.readObject();
            users=(ArrayList<Usuario>) o;            
            leer.close();
        }catch(ClassNotFoundException | IOException  ex){}
        
        users.add(user);
        try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Usuario.txt")))){
            escribir.writeObject(users);
            System.out.println("Se ha registrado su Usuario exitosamente!!\n");
            escribir.close();
        }catch(IOException e){System.out.println(e.getMessage());}
    }

    /**
     * Este metodo lee el archivo Usuario donde se encuentran anadidos todos los usuarios registrados, lee cada linea y genera un arreglo con los atributos de cada usuario 
     * para luego convertirlo en un objeto de tipo Usuario y agregarlo a una lista.
     * @return Retorna una Lista de Usuarios. 
     * @throws IOException 
     */
    public ArrayList<Usuario> crearListaUser() throws IOException{
        ArrayList<Usuario> usuarios=new ArrayList<>();
        try(ObjectInputStream leer = new ObjectInputStream(new FileInputStream(new File("Usuario.txt")))){
            Object o;
            if ((o=leer.readObject())!=null){
                usuarios=(ArrayList<Usuario>) o;           
            }
            leer.close();
        }catch(ClassNotFoundException | IOException  ex){}
        
         return usuarios;
    }

    /**
     * Este metodo permite buscar a un Usuario en el documento Usuarios.
     * @param correo: El correo nos permitira identificar al usuario en el documento
     * @param contrasena: permite saber si coinside con la que se encuentra en el registro
     * @return Retorna un dato de tipo Usuario, si este es null, significa que el ususario no existe o que el correo o contrasena estan incorrectas
     * @throws IOException 
     */
    public Usuario buscarUsuario(String correo, String contrasena) throws IOException{
        ArrayList<Usuario> users=crearListaUser();
        for (Usuario u:users){  
            if (u.getCorreo().equals(correo) && u.getContrasena().equals(contrasena))
                return u;
        }
        return null;
    }
    
    /**
     * Este metodo permite saber si existe el usuario mediante su correo, para poder evitar q otro usuario se registre con el mismo correo
     * @param correo//
     * @return//
     * @throws IOException //
     */
    public boolean existeUsuario(String correo) throws IOException{
        ArrayList<Usuario> users=crearListaUser();
        return users.stream().anyMatch((u) -> (u.getCorreo().equals(correo)));
    }    

}
