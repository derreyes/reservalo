
package InterfazUsuario;

import Diseno.*;
import java.util.*;
import Usuario.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import javax.mail.internet.ParseException;
/**
 *
 * @author Dario Erreyes
 */
public class UIMain {
        static Scanner sc = new Scanner(System.in);
        Validar v=new Validar();
        Usuario app=new Usuario();
        Archivo archivo=new Archivo();
        Establecimiento e= new Establecimiento();
        Reservar reser= new Reservar();
        
        //Calificar c=new Calificar();
        
    /**
     * @param args the command line arguments
     */

    public static void main(String[] args) throws IOException, ParseException, java.text.ParseException {
                   
      UIMain main = new UIMain();
      main.menu();
    }
    
        public void menu() throws IOException, ParseException, java.text.ParseException{
        String opcion="";
        while(!opcion.equals("3")){
              System.out.println("╔              Reservalo                    ╗");
              System.out.println("║ 1. Iniciar Sesión                         ║");
              System.out.println("║ 2. Resgistrarse                           ║");                      
              System.out.println("║ 3. Salir                                  ║");        
              System.out.println("╚                                           ╝");
              System.out.print("Ingrese opcion: ");      
              opcion = sc.nextLine().trim();
            switch (opcion){
                case "1":
                    Usuario user=iniciarsesion();
                    if (user !=null){
                        System.out.println("");
                        if (user instanceof UsuarioViajero ){
                            if(((UsuarioViajero) user).getEsGenius()){
                                System.out.println(" ---OJO--- Recuerde escribir una resena del lugar de estancia y calificarlo. Gracias por su atencion. \n");
                                    }}
                        subMenu(user);
                    }
                    
                    break;
                case "2":
                    ingresarUsuario();                    
                    break;

                case "3":    
                    System.out.println("Gracias por tu visita!");
                    break;
                default:
                    System.out.println("Opcion No valida!! \n");
            }
        }
        sc.close();
    }   

public Usuario iniciarsesion() throws IOException{
    System.out.print("Ingrese correo electrónico: ");
    String correo=sc.nextLine().trim();
    System.out.print("Ingrese contraseña: ");
    String contrasena=sc.nextLine().trim();
    Usuario u=app.buscarUsuario(correo, contrasena);
    if (u==null){
        System.out.println("El usuario ingresado no existe o el correo o la contrasena estan incorrectos.");
    }
    
    return u;
}
        
/**
 * Este metodo se encarga de registrar al usuario ya sea como usuario alojamiento como viajero, y dependiendo el tipo de usuario se crea 
 * el objeto adecuado para luego llamar al metodo anadirUsuario el cual se va a encargar de anadirlo a un archivo txt
 * @throws IOException ///
 */
public void ingresarUsuario() throws IOException{
    long tarjeta=0;
    System.out.println("1. Registro como Usuario de Alojamiento");
    System.out.println("2. Registro como Usuario de Viajeros");
    System.out.print("Ingrese una opción: ");
    String opcion=sc.nextLine().trim();
    while (!"1".equals(opcion)&&!"2".equals(opcion)){
        System.out.println("Opción no valida");
        System.out.print("Ingrese de nuevo la opción: ");
        opcion=sc.nextLine().trim();
    }
    System.out.print("Ingrese su nombre: ");
    String nombre=sc.nextLine().toUpperCase().trim() ;
    System.out.print("Ingrese correo electrónico: ");
    String correo=sc.nextLine().trim();
    while(! v.validarCorreo(correo)){
        System.out.println("Correo inválido");
        System.out.print("Ingrese de nuevo el correo electrónico: ");
        correo=sc.nextLine().trim();    
    }
    System.out.print("Ingrese contraseña(mínimo de 6 caracteres, sin caracteres especiales, al menos un número): ");
    String contrasena=sc.nextLine().trim();
    while(! v.validarContrasena(contrasena)){
       System.out.println("Contraseña no permitida");
       System.out.print("Ingrese de nuevo la contraseña: ");
       contrasena=sc.nextLine().trim();
    }   
    String Genius = "";
    if ("2".equals(opcion)){
        System.out.print("Quiere registrarse como Viajero Genius \"SI\" o \"NO\": ");
        Genius=sc.nextLine().toUpperCase().trim();
        while (!"SI".equals(Genius)&&!"NO".equals(Genius)){
             System.out.println("Opción no valida");
             System.out.print("Ingrese de nuevo la opción: ");
             Genius=sc.nextLine().toUpperCase().trim();
        }
        if ("SI".equals(Genius)){
            System.out.print("Ingrese tarjeta de crédito: "); 
            String tarjetaCredito=sc.nextLine().trim();
            while (! v.validarDatoNumerico(tarjetaCredito)){
                System.out.println("Tarjeta de credito incorreta. ");
                System.out.print("Ingrese nuevamente su tarjeta de credito: ");
                tarjetaCredito=sc.nextLine().trim();
            }
            tarjeta= Long.parseLong(tarjetaCredito);
            

        }
    }
    if ("1".equals(opcion) || "NO".equals(Genius) ) {
        System.out.print("Quiere su tarjeta de credito \"SI\" o \"NO\":");
        String respuesta=sc.nextLine().toUpperCase().trim();
        while (!"SI".equals(respuesta)&&!"NO".equals(respuesta)){
             System.out.println("Opción no valida");
             System.out.print("Ingrese de nuevo la opción: ");
             respuesta=sc.nextLine().toUpperCase().trim();
        }
        if ("SI".equals(respuesta)){
           System.out.print("Ingrese tarjeta de crédito: "); 
           String tarjetaCredito=sc.nextLine().trim();
           while (! v.validarDatoNumerico(tarjetaCredito)){
               System.out.println("Tarjeta de credito incorreta. ");
               System.out.print("Ingrese nuevamente su tarjeta de credito: ");
               tarjetaCredito=sc.nextLine().trim();
           }
            tarjeta= Long.parseLong(tarjetaCredito);
       }
        else {
            tarjeta=0;
        }
    }
    if (! app.existeUsuario(correo)){
        if ("1".equals(opcion)){
            Usuario u= new UsuarioAlojamiento(nombre, correo, contrasena, tarjeta);
            app.añadirUsuario(u);
        }
        else if("2".equals(opcion) && "SI".equals(Genius)){
            Usuario u= new UsuarioViajero(nombre, correo, contrasena, tarjeta,true);
            app.añadirUsuario(u);
        }
        else {
           Usuario u= new UsuarioViajero(nombre, correo, contrasena, tarjeta,false);
           app.añadirUsuario(u);
        }
        }
    else {
        System.out.println("El correo ingresado ya existe, intentelo nuevamente. \n ");
    }
        
    }
/**
 * Este metodo permite que el usuario consulte todos los establecimientos que se encuentran registrados en el programa
 * @throws IOException ///
 */
public void consultaEstablecimiento() throws IOException{
    ArrayList<Establecimiento> E=e.ordenarEstablecimiento();
    for (Establecimiento e:E){
            System.out.println("Nombre:"+e.getNombre()+"-Pais:"+e.getPais()+"-Ciudad:"+e.getCiudad()+"-Estrellas:"+e.getEstrellas()+"-Precio Promedio:"+e.getPomedioPrecio()+"-Calificacion"+e.getCalificacion()+"distancia: "+e.getDistAtraccion());
        }    
    
}
    /**
     * El submenu se muestra una vez que el ususario ha iniciado sesion y le da la
     *opcion de poder realizar varias acciones como calificar, registrar establecimiento,
     * buscar establecimiento y hacer reservas. Para ello recibe como parametro un usuario
     * @param user
     * @throws IOException
     * @throws ParseException
     * @throws java.text.ParseException 
     */
public void subMenu(Usuario user) throws IOException, ParseException, java.text.ParseException{
String subOpcion="";

    while(!subOpcion.equals("6")){
        System.out.printf("╔%-44s╗\n",   "Bienvenido(a)   "+user.getNombre());
        System.out.printf("║%-44s║\n","1.Realizar Reservación");
        System.out.printf("║%-44s║\n","2.Cancelar Reservacion ");
        System.out.printf("║%-44s║\n","3.Calificar ");                       
        System.out.printf("║%-44s║\n","4.Escribir reseñas ");    
        System.out.printf("║%-44s║\n","5.Anadir Establecimiento ");    
        System.out.printf("║%-44s║\n","6.Cerrar Sesion ");
        System.out.printf("╚%-44s╝\n"," ");
        System.out.print("Ingrese una opcion: ");      
        subOpcion = sc.nextLine().trim();
        switch (subOpcion){
            case "1":
                realizarReserva(user);
                break;
            case "2":
                reser.cancelarReserva(user.getNombre());
                break;
            case "3":
                System.out.println("Ingrese el nombre del establecimiento que desee calificar: ");
                String estab = sc.nextLine().trim();
                Establecimiento E= e.buscarEstablecimiento(estab);
                if (E !=null ){
                    calificar(E);
                }
                else System.out.println("El establecimiento no se encuentra registrado. \n");
                break;
            case "4":
                System.out.println("Ingrese el nombre del establecimiento que desee calificar: ");
                String es = sc.nextLine().trim();
                Establecimiento Es= e.buscarEstablecimiento(es);
                if (Es !=null ){
                    Resena(Es);
                }
                else System.out.println("El establecimiento no se encuentra registrado. \n");
                break;
            case "5":
                registrarEstablecimiento(user);
                break;
            case "6":
                System.out.println("Sesion cerrada correctamente. \n");
                break;
            default:
                System.out.println("Opción No valida!! \n");
                                     
        }
    }    
}
//Sueanny Moreno
/**
 * Este metodo se encarga de registrar un establecimiento
 * @param user////
 * @throws IOException ////
 * *Funcionamiento: 
 *Recibe como parametro un dato tipo Usuario, y la primera condicion para registar el establecimiento es que el usario 
  se haya registrado como usuarioAlojamiento, y si lo es se va a pedir cada uno de los datos del establecimiento(nombre, tipo,..., habitaciones),
  * luego se crea el objeto Establecimiento para posteriormente agregarlo a una lista.
 */
public void registrarEstablecimiento(Usuario user) throws IOException{
    if (user instanceof UsuarioAlojamiento ){
        ArrayList<Habitacion> habitacionesEstablecimiento=new ArrayList<Habitacion>();
        System.out.print("Ingrese el nombre del Establecimiento: ");
        String nombre=sc.nextLine().toUpperCase().trim();
        System.out.print("Ingrese tipo de Establecimiento: ");
        String tipo=sc.nextLine().toUpperCase().trim();
        System.out.print("Ingrese su numero de ruc: ");
        String ruc=sc.nextLine().trim();
        while(! v.validarDatoNumerico(ruc)){
            System.out.println("El ruc es invalido, ingrese un dato numerico");
            System.out.print("Ingrese un ruc valido: ");
            ruc=sc.nextLine().trim();
        }
        String longitud=null,latitud=null,city="",country="";
        while(longitud==null && latitud==null){
        System.out.print("Ingrese el pais donde se ubica: ");
        String pais=sc.nextLine().trim();
        System.out.print("Ingrese la cuidad en la que se encuentra: ");
        String ciudad=sc.nextLine().trim();
        //Para asignar las coordenadas al establecimiento
        List<ArrayList<String>> a=archivo.leerArchivo("simplemaps-worldcities-basic.csv");
        for(ArrayList<String> inf:a){
            if(inf.get(5).equalsIgnoreCase(pais)&&(inf.get(0).equalsIgnoreCase(ciudad)||inf.get(1).equals(ciudad))){
                latitud=inf.get(2);
                longitud=inf.get(3);
                city=ciudad;country=pais;
                }                
        }
        if (longitud==null && latitud==null){
        System.out.println("Los datos de pais y ciudad no se encuentran registrados, por favor intente nuevamente");
        }
        }
       System.out.print("Ingrese el numero de estrellas: ");
       String estrellas=sc.nextLine().trim(); 
       while(!estrellas.equals("1")&&!estrellas.equals("2")&&!estrellas.equals("3")&&!estrellas.equals("4")&&!estrellas.equals("5")){
        System.out.println("El dato que ingreso no es un numero entero o supera el 5.");
            System.out.print("Ingrese un numero entero de estrellas: ");
            estrellas=sc.nextLine().trim();      }
       
        String resp=null;
        int habit=0;
        do{
        System.out.println("                <<Registro de habitaciones>>            ");
            System.out.print("Ingrese el tipo de habitacion(Matrimonial, sencilla, etc): ");
            String tipoH=sc.nextLine().trim();
            System.out.print("Ingrese la cantidad de habitaciones de este tipo: ");
            String cant=sc.nextLine().trim();
            while(! v.validarDatoNumerico(cant)){
                System.out.print("Ingrese un numero de habitaciones: ");
                cant=sc.nextLine().trim();
            }
            System.out.print("Ingrese el precio de la habitacion: ");
            String precio=sc.nextLine().trim();
            while(! v.validarDatoDouble(precio)){
                System.out.print("Ingrese un valor correcto: ");
                precio=sc.nextLine().trim();
            }
            System.out.print("Ingrese la capacidad maxima de personas para la habitacion: ");
            String capacidad=sc.nextLine().trim();
            while(! v.validarDatoNumerico(capacidad)){
                System.out.print("Ingrese un numero entero de personas: ");
                capacidad=sc.nextLine().trim();
            }
            System.out.print("Ingrese la cantidad de camas de la habitacion: ");
            String camas=sc.nextLine().trim();
            while(! v.validarDatoNumerico(camas)){
                System.out.print("Ingrese un numero entero de camas: ");
                camas=sc.nextLine().trim();
            }
            while(! v.validarDatoNumerico(camas)){
                System.out.print("Ingrese un numero entero de habitaciones: ");
                camas=sc.nextLine().trim();
            }
            System.out.println("------Servicios-------");
            String[] s=archivo.leerinfor("servicios.txt");
            for (String o:s){
                System.out.println(o);
            }
            System.out.println(" ");
            System.out.println("Enumere los servicios que ofrece la habitacion");
            String servicios=sc.nextLine().trim();
            String [] servHab=servicios.split(",");
            int intcapacidad=Integer.parseInt(capacidad),intcamas=Integer.parseInt(camas);
            double dprecio=Double.parseDouble(precio);
            dprecio = Double.parseDouble(precio);

            Habitacion hab=new Habitacion(dprecio, tipoH, intcapacidad, intcamas, servHab,Integer.parseInt(cant));
            habitacionesEstablecimiento.add(hab);
            System.out.println("______Se ha registrado exitosamente su habitacion_______");
            System.out.print("Desea registrar otra habitacion(Si/No): ");
            resp=sc.nextLine().toUpperCase().trim();
            System.out.println("_____________________________________________________________");
            habit+=Integer.parseInt(cant);
            }while(resp.equals("SI"));
        //Convirtiendo los valores a su dato correspondiente
            int intestrellas = Integer.parseInt(estrellas);
            long longruc = Long.parseLong(ruc);
            double dlatitud = Double.parseDouble(latitud),dlongitud = Double.parseDouble(longitud);
            Establecimiento estab=new Establecimiento(nombre,tipo,city,country,longruc,intestrellas,habit,dlongitud,dlatitud,habitacionesEstablecimiento);
            estab.anadirEstablecimiento(estab);
            ((UsuarioAlojamiento) user).agregarEstablecimientoUsuario(user, e);    
    }
    else{
        System.out.println("Usted no se encuentra registrado como usuario de Alojamiento \n ");
    }
    }   
    
/**
 * Este metodo se va a encargar pedir  que el usuario califique al establecimiento (limpieza, ubicacion), a partir de estas calificaciones 
 * se crea el objeto calificar para luego llamar al metodo que agregará la calificacion a una lista de calificaciones del establecimiento.
 */

public void calificar(Establecimiento Est) throws IOException{
    System.out.println("Para la siguiente calificacion considere una respuesta"
            + "del 1 al 5, siendo 1 la mas baja y 5 la mas alta"+"r\n");
    System.out.print("Limpieza: ");
    String limpieza=verificarCalificacion(sc.nextLine().trim());
    System.out.print("Ubicacion: ");
    String ubicacion=verificarCalificacion(sc.nextLine().trim());
    System.out.print("Precio: ");
    String precio=verificarCalificacion(sc.nextLine().trim());
    System.out.print("Wifi: ");
    String wifi=verificarCalificacion(sc.nextLine().trim());
    System.out.print("Confort: ");
    String confort=verificarCalificacion(sc.nextLine().trim());
    System.out.print("Personal: ");
    String personal=verificarCalificacion(sc.nextLine().trim());
    Calificar cal=new Calificar(Integer.parseInt(limpieza),Integer.parseInt(ubicacion),Integer.parseInt(precio),Integer.parseInt(wifi),Integer.parseInt(confort),Integer.parseInt(personal));
    Est.anadirCalificacion(cal, Est);
    System.out.println("Su calificacion a sido guardada. \n");
}


public void realizarReserva(Usuario user) throws IOException, ParseException, java.text.ParseException{
    UIMain main = new UIMain();
    main.consultaEstablecimiento();
    System.out.println("Ingrese el nombre del Establecimiento donde desea hospedarse: ");
    String nombre= sc.nextLine().trim();
    Establecimiento establecimiento=e.buscarEstablecimiento(nombre);
    if (establecimiento != null){
        System.out.println("Desea ver las resenas de otros usuarios  ");
        String resp=sc.nextLine().toUpperCase();
        if(resp.equals("SI")){
               String resenas= establecimiento.getResena();
               for (String r:resenas.split("/")){
                   System.out.print(r+"\n");
               }
               System.out.println("");
               if (resenas.equals("")) System.out.println("No tiene resenas este establecimiento.\n");
            }
        System.out.println("\nHabitaciones del establecimiento:");
        reser.mostrarHabitaciones(establecimiento);
        System.out.println("Eliga el tipo de habitacion: ");
        String tipo=sc.nextLine().trim();
        Habitacion H=e.buscarHabitaciones(establecimiento, tipo);
        if (H!=null){
            int cantidad=H.getCantidad() +1;int z=0;
            while (cantidad > H.getCantidad()){
               
                System.out.println("Elija la cantidad de habitaciones q desea de ese tipo: ");
                String cant=sc.nextLine().trim();
                while(! v.validarDatoNumerico(cant)){
                System.out.println("El dato que ingreso no es un numero entero ");
                System.out.print("Ingrese un numero entero de estrellas: ");
                cant=sc.nextLine().trim();      }
                cantidad= Integer.parseInt(cant);
                if (z!=0) System.out.println("El numero ingresado sobrepasa a la cantidad de habitaciones disponibles. \n");
                z++;
            }
            System.out.println("Elija el numero de noches: ");
                String noches=sc.nextLine().trim();
                while(! v.validarDatoNumerico(noches)){
                System.out.println("El dato que ingreso no es un numero entero ");
                System.out.print("Ingrese un numero entero de estrellas: ");
                noches=sc.nextLine().trim();      }
                
            System.out.println("Ingrese la fechade la reservacion (Formato: DD/MM/AA): ");
            String fecha=sc.nextLine().trim();
           if (user.getTarjetaCredito()==0){
               System.out.print("Ingrese su tarjeta de crédito: "); 
                String tarjetaCredito=sc.nextLine().trim();
                while (! v.validarDatoNumerico(tarjetaCredito)){
                System.out.println("Tarjeta de credito incorreta. ");
                System.out.print("Ingrese nuevamente su tarjeta de credito: ");
                tarjetaCredito=sc.nextLine().trim();
                }
                Long tarjeta= Long.parseLong(tarjetaCredito);
                 ArrayList<Usuario> userss= app.crearListaUser();
                ArrayList<Usuario> USSS= new ArrayList<>();
                for (Usuario o:userss){
                    if (!(user.getCorreo().equals(o.getCorreo()))) {
                        USSS.add(o);
                    }
                    else{
                        user.setTarjetaCredito(tarjeta);
                        USSS.add(o);
                    }
                }

                try(ObjectOutputStream escribir = new ObjectOutputStream(new FileOutputStream( new File("Usuario.txt")))){
                escribir.writeObject(USSS);
                escribir.close();
                }catch(IOException s){}
           }
            
            reser.reservar(reser,user, establecimiento, fecha, tipo, cantidad, Integer.parseInt(noches));
            
            
        }
    }
    else System.out.println("El establecimiento no se encuentra registrado. \n");
}


/**
 * este metodo se encarga de validar la calificacion (1-2-3-4-5) dada por el usuario, este metodo es utilizado en el metodo calificar()
 * @param calif///
 * @return ///
 */
public String verificarCalificacion(String calif){
    while (!calif.equals("1")&&!calif.equals("2")&&!calif.equals("3")&&!calif.equals("4")&&!calif.equals("5")){
        System.out.println("Dato no valido!!");
        System.out.print("Ingrese de nuevo calificación:");
        calif=sc.nextLine().trim();
    }
    return calif;
}

/**
 * Este metodo nos permite realizar la reserva, si el usuario lo desea.
 * @param es
 * @throws IOException 
 */
public void Resena(Establecimiento es) throws IOException{
    System.out.println("Escriba su resena del establecimiento: ");
    String resena=sc.nextLine();
    es.anadirResena(es, resena);
    System.out.println("Su resena a sido guardada. \n");
}
}




